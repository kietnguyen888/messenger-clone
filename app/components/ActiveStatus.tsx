'use client';

import useActiveChannel from "../hooks/usActiveChannel";


const ActiveStatus = () => {
  useActiveChannel();

  return null;
}
 
export default ActiveStatus;
